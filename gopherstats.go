package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"

	"github.com/gobwas/glob"
)

const (
	PACKAGES = `$packages["`
	OFFSET   = len(PACKAGES)
)

var (
	topN      = flag.Int("topN", 20, "print the top N packages by size")
	printUses = flag.Bool("usedBy", false, "print the packages which use each listed package")
	printDeps = flag.Bool("deps", false, "print the dependencies of each listed package")
	showAll   = flag.Bool("all", false, "print all packages, ignoring the value of -topN")
	tree      = flag.Bool("tree", false, "print dependencies or uses recursively as a tree")
	verbose   = flag.Bool("v", false, "show verbose output")

	pPattern = flag.String("package", "*", "show stats for matching packages (e.g. crypto/*)")
	pGlob    glob.Glob
)

func main() {
	flag.Parse()
	pGlob = glob.MustCompile(*pPattern, '.')
	for _, file := range flag.Args() {
		process(file)
	}
}

type PackageSize struct {
	name string
	size int
}
type PackageSizes []PackageSize

// Len is the number of elements in the collection.
func (a PackageSizes) Len() int {
	return len(a)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (a PackageSizes) Less(i, j int) bool {
	return a[j].size < a[i].size
}

// Swap swaps the elements with indexes i and j.
func (a PackageSizes) Swap(i, j int) {
	tmp := a[j]
	a[j] = a[i]
	a[i] = tmp
}

func process(file string) {
	f, err := os.Open(file)
	check(err)

	defer f.Close()

	// Read lines and build map of package size
	fmt.Printf("\nPackages for %s:\n", file)
	sizes := make(map[string]int)
	deps := make(map[string]map[string]struct{})
	reader := bufio.NewReader(f)

	newline := true
	packageName := "<init>"
	size := 0
	line, isPrefix, err := reader.ReadLine()
	for err == nil {
		str := string(line)
		if newline && strings.HasPrefix(str, PACKAGES) {
			// Store current package stats
			sizes[packageName] = size
			size = 0
			end := strings.Index(str[OFFSET:], `"`) + OFFSET
			packageName = str[OFFSET:end]

			str = str[end:]
		}

		pos := strings.Index(str, PACKAGES)

		for pos >= 0 {
			start := pos + 11
			end := strings.Index(str[start:], `"`) + start
			depName := str[start:end]

			if depName != packageName {
				if deps[packageName] == nil {
					deps[packageName] = make(map[string]struct{})
				}
				deps[packageName][depName] = struct{}{}
			}

			if end < len(str) {
				str = str[end:]
				pos = strings.Index(str, PACKAGES)
			} else {
				break
			}
		}

		newline = !isPrefix
		size = size + len(line)
		line, isPrefix, err = reader.ReadLine()
	}

	usedBy := make(map[string]map[string]struct{})
	for p, deps := range deps {
		for dep := range deps {
			if usedBy[dep] == nil {
				usedBy[dep] = make(map[string]struct{})
			}
			usedBy[dep][p] = struct{}{}
		}
	}

	// Sort by size
	// TODO allow user to choose sort order
	var sortedPackages []PackageSize
	for k, v := range sizes {
		sortedPackages = append(sortedPackages, PackageSize{k, v})
	}
	sort.Sort(PackageSizes(sortedPackages))

	count := *topN
	total := len(sortedPackages)
	if total < count || *showAll {
		count = total
	}

	i := 0
	for _, s := range sortedPackages {
		name := s.name
		size := s.size
		i++

		if !pGlob.Match(name) {
			continue
		}

		if count == 0 {
			break
		}
		count -= 1

		if *printUses || *printDeps {
			fmt.Println()
		}
		fmt.Printf("%10d kb   %s\n", size/1024, name)

		if *printUses {
			fmt.Println("\nUsed by:")

			printNames(name, 1, sizes, usedBy)
			fmt.Println()
		}

		if *printDeps {
			fmt.Println("\nDepends on:")

			printNames(name, 1, sizes, deps)
			fmt.Println()
		}
	}

	if i < total {
		total := 0
		found := 0
		for _, s := range sortedPackages[i:] {
			if !pGlob.Match(s.name) {
				continue
			}
			total += s.size
			found += 1
		}
		if found > 0 {
			fmt.Printf("\n%d more packages totalling %d kb, use gopherstats -all or -topN to show them\n", found, total/1024)
		}
	}
}

func printNames(parent string, depth int, sizes map[string]int, packages map[string]map[string]struct{}) {

	if depth >= 100 {
		panic("Depth exceeded 100")
	}

	// Collect packages
	var names []string
	for d := range packages[parent] {
		names = append(names, d)
	}

	// Sort by name
	sort.Strings(names)

	width := 0
	for _, k := range names {
		if len(k) > width {
			width = len(k)
		}
	}

	// Print columns
	cols := 80 / (width + 1)
	if cols == 0 {
		cols = 1
	}

	if *tree || *verbose {
		cols = 1
	}

	col := 0
	for _, d := range names {
		if col%cols == 0 {
			fmt.Println()
		}
		if *verbose {
			fmt.Printf(fmt.Sprintf("%%s%%10d kb   %%-%ds", width), strings.Repeat("  ", depth), sizes[d]/1024, d)
		} else {
			fmt.Printf(fmt.Sprintf("%%s%%-%ds", width), strings.Repeat("  ", depth), d)
		}
		if *tree {
			printNames(d, depth+1, sizes, packages)
		}
		col += 1
	}
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
