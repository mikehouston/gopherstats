# Gopherstats #

Print information about JavaScript compiled from Go by GopherJS

## Usage

List largest packages in bundle:
~~~
$ gopherstats app.js
Packages for app.js:
       511 kb   net/http
       378 kb   net
       306 kb   crypto/tls
       166 kb   reflect
       140 kb   unicode
       135 kb   fmt
       127 kb   html/template
       126 kb   text/template/parse
       126 kb   text/template
       116 kb   strings
       112 kb   math/big
       106 kb   crypto/elliptic
       102 kb   regexp
        91 kb   regexp/syntax
        91 kb   crypto/x509
        86 kb   time
        73 kb   encoding/json
        70 kb   strconv
        68 kb   encoding/asn1
        67 kb   syscall

60 more packages totalling 885kb, use gopherstats -all or -topN to show them
~~~

List only the top N packages:
~~~
$ gopherstats -topN 3 app.js

Packages for app.js:
       511 kb   net/http
       378 kb   net
       306 kb   crypto/tls

77 more packages totalling 2696kb, use gopherstats -all or -topN to show them
~~~

Show a specific package's details:
~~~
$ gopherstats -package time app.js     

Packages for app.js:
        86 kb   time
~~~

Show package dependencies:
~~~
$ gopherstats -package time -deps app.js

Packages for app.js:

        86 kb   time

Depends on:

  errors                               github.com/gopherjs/gopherjs/js    
  github.com/gopherjs/gopherjs/nosync  runtime                            
  syscall
~~~

Show packages which use a package:
~~~
$ gopherstats -package time -usedBy app.js

Packages for app.js:

        86 kb   time

Used by:

  <init>                                                
  compress/gzip                                        
  context                                              
  crypto/tls                                           
  crypto/x509                                          
  crypto/x509/pkix                                     
  encoding/asn1                                        
  github.com/gopherjs/websocket                        
  github.com/tj/go-debug                               
  golang.org/x/net/context                             
  golang.org/x/net/http2                               
  golang.org/x/net/internal/timeseries                 
  golang.org/x/net/trace                               
  google.golang.org/grpc                               
  google.golang.org/grpc/transport                     
  honnef.co/go/js/dom                                  
  io/ioutil                                            
  log                                                  
  net                                                  
  net/http                                             
  net/http/httptrace                                   
  os       
~~~

Display a tree of dependencies or usage:
~~~
$ gopherstats -package log -usedBy -tree app.js

Packages for app.js:

        16 kb   log

Used by:

  github.com/go-humble/router                      
  github.com/golang/protobuf/proto                 
  golang.org/x/net/http2                           
  golang.org/x/net/internal/timeseries             
    golang.org/x/net/trace
  golang.org/x/net/trace                           
  net/http                                         
    golang.org/x/net/http2          
    golang.org/x/net/trace      
~~~

## Installation

~~~
go get bitbucket.org/mikehouston/gopherstats
~~~